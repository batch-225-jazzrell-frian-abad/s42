// querySelector() method returns the first element that matches a selector

const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");

const txtLastName = document.querySelector("#txt-last-name");
const spanlastName = document.querySelector("#span-last-name");

// addEventLister() - method attaches an event handler to an element
// keyup - A keyboard key is released after being pushed

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
})



txtFirstName.addEventListener('keyup', (event) =>{
	console.log(event.target);
	console.log(event.target.value);
})



txtLastName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
})
